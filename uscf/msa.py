"""The unofficial USCF MSA module"""


import urllib
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as BS


class profile:
    # Values shared by all profiles
    """        
        List of urls it uses:
          - msa.memberProfile
            : http://www.uschess.org/msa/thin3.php?
          - msa.Main
            : http://www.uschess.org/msa/MbrDtlMain.php?
          - msa.Milestones
            : http://www.uschess.org/msa/iMbrDtlMilestones.php?
          - msa.RatingSupplement
            : http://www.uschess.org/msa/MbrDtlRtgSupp.php?
          - msa.TournamentDirector
            : http://www.uschess.org/msa/MbrDtlTnmtDir.php?
          - msa.TournamentHosting
            : http://www.uschess.org/msa/MbrDtlTnmtHst.php?

        Each url requires the USCF member ID.

        msa.msa_scripts - Returns a list of all urls

    """
    url = 'http://www.uschess.org/msa/'
    msa_scripts = {
        'Profile': url + 'thin3.php?',
        'Main': url + 'MbrDtlMain.php?',
        'Milestones': url + 'iMbrDtlMilestones.php?',
        'RatingSupplement': url + 'MbrDtlRtgSupp.php?',
        'TournamentDirector': url + 'MbrDtlTnmtDir.php?',
        'TournamentHosting': url + 'MbrDtlTnmtHst.php?',
        }

    def __init__(self, id):
        self.id = id
        self.profile_objects = {}

    @staticmethod
    def bsObject(url):
        """bsObject(url)

        Returns a BeautifulSoup object

        """
        header = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,"
                      "*/*;q=0.8",
            "User-Agent": "Mozilla/5.0 (X11; Linux i686; rv:38.0) Gecko/20100101"
                      "Firefox/38.0 Iceweasel/38.8.0"
        }
    
        req = urllib.request.Request(url=url, headers=header)
        res = urllib.request.urlopen(req)
    
        soup = BS(res, 'html.parser')

        return soup


    def retrieve(self, script=None):
        """retrieve(self, script)

        Requests script (webpage) and stores a BeautifulSoup object in profile_objects.
        No arguments requests all of them.
        
        Scripts are (as defined by profile class):
          Profile - Current USFC rating status 
          Main - Overview USCFC status
          Milestons - Overview Category acheivements
          RatingSupplement - Overview of Ratings
          TournamentDirector - Overview of tournaments by director
          TournamentHosting - Overview of tournament participation 

        """
        if script is None:
            for script in self.msa_scripts:
                self.retrieve(script)
        else:
            data = self.bsObject(url=self.msa_scripts[script]+self.id)
            self.profile_objects.update({script: data})


    def getProfile(self):
        """getProfile(self)

        Returns a JSON of the member's current USCF rating status

        Ex.

        [
          {
            "blitz_rating": "Unrated",
            "uscf_id": "12743305",
            "expires": "2099-12-31",
            "fide_id": "2020009 G USA",
            "fide_rating": "2823 2016-12-01",
            "quick_rating": "2310* 2016-12-01",
            "name": "FABIANO CARUANA",
            "reg_rating": "2891* 2016-12-01",
            "state_country": "FL"
          }
        ]

        """
        #TODO should these getters request the webpage if it hasn't been retrived already?
        data = self.profile_objects['Profile']
    
        inputs = data.findAll('input', attrs={'value': True})
    
        value = []
        for item in inputs:
            value.append(item['value'])
    
        json_data =  [{
                    'uscf_id': value[0],
                    'expires': value[1],
                    'name': value[2],
                    'reg_rating': value[3],
                    'quick_rating': value[4],
                    'blitz_rating': value[5],
                    'state_country': value[6],
                    'fide_id': value[7],
                    'fide_rating': value[8]
                   }]
    
        return json_data


    def getMain(self):
        """getMain(self)

        Returns a JSON of main

        """
        pass


    def getMilestones(self):
        """getMilestones(self)

        Returns a JSON of the milestones

        Ex.

        [
          {
            "2001-02-12": {
              "section": "1",
              "notes": "",
              "milestone": "2nd Category Title",
              "event_id": "200102128480"
            },
            "2000-08-04": {
              "section": "1",
              "notes": "",
              "milestone": "25 Quick/Dual Rated Wins",
              "event_id": "200008049440"
            },
            ...
        ]

        """
        json_data = []
        end_date_count = 0

        data = self.profile_objects.get('Milestones')

        # Calculate how many sources and a count of 0 for none
        count = 0
        try:

            inputs = data.findAll('table')[6]
        except (IndexError):

            json_data.append({ 'count': count })

            return json_data

        for x in inputs.findAll('tr'):
            count += 1
        json_data.append({'count': count-4})

        # -1 to remove headers and -3 information rows at the bottom
        for x in range(1, count-3):
            td = inputs.findAll('tr')[x].findAll('td')

            end_date = td[1].text

            if end_date in json_data[0].keys():
                end_date = end_date + '_' + str(end_date_count)
                end_date_count += 1

            json_data[0].update({ end_date : {
                               'milestone': td[2].text,
                               'event_id': td[3].text,
                               'section': td[4].text,
                               'notes': td[5].text,
                              }}
            )

        return json_data        


    def getRatingSupplement(self):
        """getRatingSupplement(self)

        Returns a JSON of the rating supplement

        Ex.

        [
          {
            "count": 115,
            "2001-08": {
              "only_quick_rating": "---",
              "notes": "",
              "quick_rating": "1753",
              "blitz_rating": "---",
              "regular_rating": "1762",
              "only_blitz_rating": "---"
            },
            "2011-08": {
              "only_quick_rating": "---",
              "notes": "",
              "quick_rating": "2267",
              "blitz_rating": "---",
              "regular_rating": "2786",
              "only_blitz_rating": "---"
            },
            ...
        ]

        """
        json_data = []

        data = self.profile_objects['RatingSupplement']

        inputs = data.findAll('table')[6]

        # Calculate how many sources
        count = 0
        for x in inputs.findAll('tr'):
            count += 1
        json_data.append({'count': count-1})

        # -1 to remove headers
        for x in range(1, count):
            td = inputs.findAll('tr')[x].findAll('td')

            json_data[0].update({ td[1].text : {
                               'regular_rating': td[2].text,
                               'quick_rating': td[3].text,
                               'blitz_rating': td[4].text,
                               'only_quick_rating': td[5].text,
                               'only_blitz_rating': td[6].text,
                               'notes': td[7].text,
                              }}
            )
        
        return json_data


    def getTournamentDirector(self):
        """getTournamentDirector(self)

        Returns a JSON of the tournament director

        Ex.

        [
          {
            "total_sections_worked": {
              "count": "39",
              "notes": "(in 30 events since Jan. 1, 1991)"
            },
            "tournaments_as_chief_director": {
              "count": "27",
              "notes": "(since Jan. 1, 1991)"
            },
            ...
        ]

        """
        json_data = []

        data = self.profile_objects.get('TournamentDirector')

        inputs = data.findAll('table')[5]

        json_data.append({
                          'certification_level': inputs.findAll('tr')[0].findAll('td')[1].findAll('b')[0].text,
                          'tournaments_as_chief_director': {
                                                            'count' : inputs.findAll('tr')[1].findAll('td')[1].findAll('b')[0].text,
                                                            'notes' : inputs.findAll('tr')[1].findAll('td')[1].findAll('i')[0].text
                                                           },
                          'sections_directed': { 
                                                 'count' : inputs.findAll('tr')[2].findAll('td')[1].findAll('b')[0].text,
                                                 'notes' : inputs.findAll('tr')[2].findAll('td')[1].findAll('i')[0].text
                                               },
                          'other_events_worked': { 
                                                  'count' : inputs.findAll('tr')[3].findAll('td')[1].findAll('b')[0].text,
                                                  'notes' : inputs.findAll('tr')[3].findAll('td')[1].findAll('i')[0].text
                                                 },
                          'total_sections_worked': {
                                                    'count' : inputs.findAll('tr')[4].findAll('td')[1].findAll('b')[0].text,
                                                    'notes' : inputs.findAll('tr')[4].findAll('td')[1].findAll('i')[0].text
                                                   }
                          }
        )

        return json_data


    def getTournamentHosting(self):
        """getTournamentHosting(self)

        Returns a JSON of the tournament hosting

        Ex.

        [
          {
            "2015-01-05": {
              "event_name": "TATA STEEL MASTERS (NED)",
              "regular_rating": "2900 => 2887",
              "quick_rating": "\u00a0",
              "blitz_rating": "\u00a0",
              "section_name": "1: TATA STEEL MASTERS",
              "event_id": "201501058102 "
            },
            "2013-10-20": {
              "event_name": "27TH EUROPEAN CUP (GRE)",
              "regular_rating": "2875 => 2857",
              "quick_rating": "\u00a0",
              "blitz_rating": "\u00a0",
              "section_name": "1: 27TH EUROPEAN CUP",
              "event_id": "201310205502 "
            },
            ...
        ]

        """
        json_data = []
        end_date_count = 0

        data = self.profile_objects['TournamentHosting']

        inputs = data.findAll('table')[6]
    
        # Calculate how many sources
        count = 0
        for x in inputs.findAll('tr'):
            count += 1
        json_data.append({'count': count-1})

        # -1 to remove headers
        for x in range(1, count):
            td = inputs.findAll('tr')[x].findAll('td')

            try:
                event_id = td[0].find('br').text
            except (AttributeError):
                print('Cache has changed. Must retrieve.')
                return

            td[0].find('br').extract()
            end_date = td[0].text

            section_name = td[1].find('br').text

            td[1].find('br').extract()
            event_name = td[1].text

            regular_rating = td[2].text
            quick_rating = td[3].text
            blitz_rating = td[4].text

            if end_date in json_data[0].keys():
                end_date = end_date + '_' + str(end_date_count)
                end_date_count += 1

            json_data[0].update({ end_date  :
                             {
                              'event_id': event_id,
                              'event_name': event_name,
                              'section_name': section_name,
                              'regular_rating': regular_rating,
                              'quick_rating': quick_rating,
                              'blitz_rating': blitz_rating
                             }}
            )
    
        return json_data
