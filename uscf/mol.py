"""The unofficial Member's Only USCF login module"""


import requests
from bs4 import BeautifulSoup as BS


class mol(object):

    # USCF url
    domain_url = 'https://secure2.uschess.org'

    # Member's Only Login
    login_url = 'https://secure2.uschess.org/MembersOnly/MO_check.php'

    # Ratings Supplement Files (Regular/Quick ratings)
    reg_quick = 'https://secure2.uschess.org/MembersOnly/downloadQ.php'

    # Ratings Supplement Files (Regular/Blitz ratings)
    reg_blitz = 'https://secure2.uschess.org/MembersOnly/downloadB.php'


    def __init__(self, uid, pin):

        self.uid = uid
        self.pin = pin
        self.session = None
        self.rqlist = []
        self.rblist = []

    @staticmethod
    def bsObject(data):
        """bsObject(url)

        Returns a BeautifulSoup object

        """
        return BS(data, 'html.parser')

    def dictFile(self, url):

        data = self.session.get(url)
        data_bsobj = self.bsObject(data.text)

        datalist = []
        for x in data_bsobj.findAll('a', href=True):
            if 'supplements' in x['href']:
                datalist.append( { x['href'].split('/')[-1]:
                                   { 
                                    'title': x.text,
                                    'href': x['href']
                                   }
                                 } 
                )

        return datalist


    def login(self):

        # Create session
        self.session = requests.session()

        # Login
        data = {'MoUscfld1': self.uid, 'pin': self.pin}
        self.session.post(mol.login_url, data)

    def reg_quick_list(self):

        self.rqlist = self.dictFile(mol.reg_quick)

        return self.rqlist

    def reg_blitz_list(self):

        self.rblist = self.dictFile(mol.reg_blitz)

        return self.rblist

    def download(self, filename):

        self.allList = []
        self.allList = self.rqlist + self.rblist

        for item in self.allList:
            for key in item.keys():
                if key == filename:
                    print('Downloading '+ key +'...')
                    buf = self.session.get(mol.domain_url + '/supplements/' + key)

                    print('Creating zip file...')
                    f = open(key, 'wb')
                    f.write(buf.content)
                    f.close()
