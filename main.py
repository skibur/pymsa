#!/usr/bin/env python3

import json
import sys
from uscf import msa

try:
    if sys.argv[1]:
        chess_id = sys.argv[1]
    else:
        print("Please provide USCF id.")
        exit(1)

except IndexError:
    print("Please provide USCF id.")
    exit(1)

player = msa.profile(chess_id)

try:
    print('Profile')
    player.retrieve('Profile')
    # should these getters request the webpage if it hasn't been requested yet?
    data = player.getProfile()
    print(json.dumps(data, indent=2))

    print('Rating Supplement')
    player.retrieve('RatingSupplement')
    data = player.getRatingSupplement()
    print(json.dumps(data, indent=2))

    print('Tournament Hosting')
    player.retrieve('TournamentHosting')
    data = player.getTournamentHosting()
    print(json.dumps(data, indent=2))

    print('Milestones')
    player.retrieve('Milestones')
    data = player.getMilestones()
    print(json.dumps(data, indent=2))

    print('TournamentDirector')
    player.retrieve('TournamentDirector')
    data = player.getTournamentDirector()
    print(json.dumps(data, indent=2))
except IndexError:
    print("Something went wrong.")
    exit(1)


